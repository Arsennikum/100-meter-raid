// все значения размеров в метрах, если не указано иное
const config = {
    circleRadius: 100,
    key: "rutnpt3272",
    workplaceSize: 300,
    resultCount: 50,
    refreshDistance: 1000,
    markersPageSize: 10000,
    minZoom: 14,
    educationSearchQueries: ['школа', 'детский сад', 'развитие детей', 'вуз', 'автошкола',
        'мотошкола', 'переподготовка', 'Обучение рабочим профессиям', 'Военно-патриотические клубы',
        'Детские клубы', 'техникум', 'колледж', 'академии', 'институт'],
    shopSearchQueries: ['Продукты', 'табак'],
    middleDistance: 170,
    regionZoom: 12,
    markerSelectEvent: 'contextmenu',
    localization: {
        btnRouteName: "Создать маршрут",
        linkName: "Ссылка на маршрут"
    }
};


export default config;